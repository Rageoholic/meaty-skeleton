#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <kernel/tty.h>

#include "vga.h"

static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;
static uint16_t* const VGA_MEMORY = (uint16_t*) 0xB8000;

static size_t TermRow;
static size_t TermColumn;
static uint8_t TermColor;
static uint16_t* TermBuffer;

void TermInitialize(void)
{
  TermRow = 0;
  TermColumn = 0;
  TermColor = vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK);
  TermBuffer = VGA_MEMORY;
  for (size_t y = 0; y < VGA_HEIGHT; y++)
  {
    for (size_t x = 0; x < VGA_WIDTH; x++)
    {
      const size_t index = y * VGA_WIDTH + x;
      TermBuffer[index] = vga_entry(' ', TermColor);
    }
  }
}

void TermSetColor(uint8_t color)
{
  TermColor = color;
}

void TermPutEntryAt(unsigned char c, uint8_t color, size_t x, size_t y)
{
  const size_t index = y * VGA_WIDTH + x;
  TermBuffer[index] = vga_entry(c, color);
}

void TermPutChar(char c)
{
  unsigned char uc = c;
  TermPutEntryAt(uc, TermColor, TermColumn, TermRow);
  if (++TermColumn == VGA_WIDTH)
  {
    TermColumn = 0;
    if (++TermRow == VGA_HEIGHT)
    {
      TermRow = 0;
    }
  }
}

void TermWrite(const char* data, size_t size)
{
  for (size_t i = 0; i < size; i++)
  {
    TermPutChar(data[i]);
  }
}

void TermWriteString(const char* data)
{
  TermWrite(data, strlen(data));
}
