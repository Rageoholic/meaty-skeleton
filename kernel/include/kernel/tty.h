#ifndef _KERNEL_TTY_H
#define _KERNEL_TTY_H

#include <stddef.h>
#include <stdint.h>

void TermInitialize(void);

void TermSetColor(uint8_t color);

void TermPutEntryAt(unsigned char c, uint8_t color, size_t x, size_t y);

void TermPutChar(char c);

void TermWrite(const char* data, size_t size);

void TermWriteString(const char* data);

#endif
