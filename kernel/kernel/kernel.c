#include <stdio.h>

#include <kernel/tty.h>

void KernelMain(void)
{
  TermInitialize();
  printf("Hello, kernel World!\n");
}
