#!/bin/sh
set -e
. ./build.sh

mkdir -p isodir
mkdir -p isodir/boot
mkdir -p isodir/boot/grub

cp sysroot/boot/parengus.kernel isodir/boot/parengus.kernel
cat > isodir/boot/grub/grub.cfg << EOF
menuentry "myos" {
	multiboot /boot/parengus.kernel
}
EOF
grub-mkrescue -o parengus.iso isodir
